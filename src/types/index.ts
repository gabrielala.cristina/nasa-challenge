export interface Card {
  nasaId: string;
  image: string;
  title: string;
  description: string;
  location: string | undefined;
  photographer: string | undefined;
  creationDate: Date;
  keywords: string[];
}
