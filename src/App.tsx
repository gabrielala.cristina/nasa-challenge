import "./App.css";
import { Route, Routes } from "react-router-dom";
import ShowPage from "./pages/ShowPage/ShowPage";
import SearchPage from "./pages/SearchPage/SearchPage";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<SearchPage />} />
        <Route path="/:nasaId" element={<ShowPage />} />
      </Routes>
    </div>
  );
}

export default App;
