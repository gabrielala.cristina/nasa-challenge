import { Store } from "pullstate";
import { Card } from "../types";

export const AppStore = new Store({
  selectedItem: {
    nasaId: "",
    image: "",
    title: "",
    description: "",
    location: "",
    photographer: "",
    creationDate: new Date(""),
    keywords: [],
  } as Card,
});
