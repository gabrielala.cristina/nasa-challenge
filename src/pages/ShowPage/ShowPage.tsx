import { useEffect } from "react";
import { AppStore } from "../../store/store";
import { useNavigate } from "react-router-dom";
import { Card } from "antd";
import { formatDate } from "../../utils";

const ShowPage: React.FC = () => {
  const selectedItem = AppStore.useState((s) => s.selectedItem);
  const navigate = useNavigate();
  const formattedDate = formatDate(selectedItem.creationDate);
  useEffect(() => {
    if (Object.keys(selectedItem).length === 0) navigate("/");
  }, [selectedItem, navigate]);

  return (
    <Card
      style={{ width: "70%", height: "100%" }}
      title={selectedItem.title}
      extra={<a href="/">Back</a>}
      cover={
        <img style={{ width: "60%", margin: 20 }} src={selectedItem.image} />
      }
    >
      <p>
        <b>Description: </b>
        {selectedItem.description ? selectedItem.description : "N/A"}
      </p>
      <p>
        <b>Photographer: </b>
        {selectedItem.photographer ? selectedItem.photographer : "N/A"}
      </p>
      <p>
        <b>Location:</b> {selectedItem.location ? selectedItem.location : "N/A"}
      </p>
      <p>
        <b>Creation date: </b>
        {formattedDate ? formattedDate : "N/A"}
      </p>
      <p>
        <b>Keywords: </b>
        {selectedItem.keywords.map((keyword: string) => (
          <span>{keyword} </span>
        ))}
      </p>
    </Card>
  );
};

export default ShowPage;
