import { Input, DatePicker, Form, Button } from "antd";
import type { Dayjs } from "dayjs";
import dayjs from "dayjs";
import { useState } from "react";
import { mapItems } from "../../utils";
import ResultCard from "./ResultCard";
import { Card } from "../../types";

const { RangePicker } = DatePicker;
type RangeValue = [Dayjs | null, Dayjs | null] | null;

const SearchPage: React.FC = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);

  const disabledDate = (current: Dayjs) => {
    return current && dayjs(current).isAfter(dayjs(), "year");
  };

  const handleSearch = async (values: any) => {
    const selectedYears: number[] | undefined = values.yearRange?.map(
      (date: any) => date?.year() || 0
    );
    let apiUrl = `https://images-api.nasa.gov/search?q=${values.searchInput}&media_type=image&page=1&page_size=20`;
    if (selectedYears && selectedYears[0] && selectedYears[1]) {
      apiUrl += `&year_start=${selectedYears[0]}&year_end=${selectedYears[1]}`;
    }
    try {
      const response = await fetch(apiUrl);
      const data = await response.json();
      setData(data.collection.items.map(mapItems));
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div style={{ marginTop: 50, width: "70%" }}>
      <Form name="searchForm" form={form} onFinish={handleSearch}>
        <Form.Item name="yearRange">
          <RangePicker picker="year" disabledDate={disabledDate} />
        </Form.Item>
        <div style={{ display: "flex" }}>
          <Form.Item
            name="searchInput"
            rules={[{ required: true, message: "Search input is required" }]}
          >
            <Input placeholder="Search for images" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Search
            </Button>
          </Form.Item>
        </div>
      </Form>
      {data.length > 0 &&
        data.map((item: Card) => (
          <div key={item.nasaId}>
            <ResultCard item={item} />
          </div>
        ))}
    </div>
  );
};

export default SearchPage;
