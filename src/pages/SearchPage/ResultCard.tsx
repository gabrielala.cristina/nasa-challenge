import { Card } from "antd";
import { Card as CardType } from "../../types";
import { useNavigate } from "react-router-dom";
import { AppStore } from "../../store/store";

const { Meta } = Card;

const ResultCard = (item: any) => {
  const card = item.item as CardType;
  const navigate = useNavigate();

  const navigateToItem = async () => {
    navigate(`/${card.nasaId}`);
    AppStore.update((s) => {
      s.selectedItem = card;
    });
  };

  return (
    <Card
      data-testid="card"
      hoverable
      style={{ marginBottom: 20 }}
      cover={
        <img style={{ width: 100, height: 100, margin: 10 }} src={card.image} />
      }
      onClick={() => navigateToItem()}
    >
      <Meta title={card.title} />
      <p>
        <b>Photographer: </b> {card.photographer ? card.photographer : "N/A"}
      </p>
      <p>
        <b>Location: </b>
        {card.location ? card.location : "N/A"}
      </p>
    </Card>
  );
};

export default ResultCard;
