import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import ResultCard from "../ResultCard";

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...(jest.requireActual("react-router-dom") as any),
  useNavigate: () => mockedUsedNavigate,
}));

const cardData = {
  nasaId: "1234",
  title: "Title",
  photographer: "John Doe",
  location: "Somewhere",
  image: "image-url",
};

test("renders ResultCard with correct data", () => {
  render(<ResultCard item={cardData} />);

  expect(screen.getByText("Title")).toBeInTheDocument();
  expect(screen.getByText("John Doe")).toBeInTheDocument();
  expect(screen.getByText("Somewhere")).toBeInTheDocument();
});

test("navigates to the correct URL when clicked", () => {
  render(<ResultCard item={cardData} />);

  const card = screen.getByTestId("card");
  fireEvent.click(card);

  // Check if navigate was called with the correct URL
  expect(mockedUsedNavigate).toHaveBeenCalledTimes(1);
  expect(mockedUsedNavigate).toHaveBeenCalledWith("/1234");
});
