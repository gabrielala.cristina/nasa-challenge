import "@testing-library/jest-dom";
import {
  render,
  screen,
  fireEvent,
} from "@testing-library/react";
import SearchPage from "../SearchPage";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: (query: any) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: () => {},
    removeListener: () => {},
    addEventListener: () => {},
    removeEventListener: () => {},
    dispatchEvent: () => {},
  }),
});

test("renders SearchPage", () => {
  render(<SearchPage />);

  expect(screen.getByPlaceholderText("Start year")).toBeInTheDocument();
  expect(screen.getByPlaceholderText("End year")).toBeInTheDocument();
  expect(screen.getByPlaceholderText("Search for images")).toBeInTheDocument();
  expect(screen.getByText("Search")).toBeInTheDocument();
});

test("should be able to click the search button", async () => {
  render(<SearchPage />);

  const searchInput = screen.getByPlaceholderText("Search for images");
  const searchButton = screen.getByText("Search");

  fireEvent.change(searchInput, { target: { value: "space" } });
  fireEvent.click(searchButton);
});
