export const mapItems = (item: any) => {
  return {
    nasaId: item.data[0].nasa_id,
    image: item.links[0].href,
    title: item.data[0].title,
    description: item.data[0].description,
    location: item.data[0].location,
    photographer: item.data[0].photographer,
    creationDate: item.data[0].date_created,
    keywords: item.data[0].keywords,
  };
};

export const formatDate = (date: Date) => {
  const originalDate = new Date(date);
  const year = originalDate.getUTCFullYear();
  const month = originalDate.getUTCMonth() + 1;
  const day = originalDate.getUTCDate();
  return `${day}/${month}/${year}`;
};
